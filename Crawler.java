import java.util.Arrays;
import java.io.*;
import com.google.gson.Gson;
import java.util.HashSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Callable;

public class Crawler implements Callable<Crawler.Results> {
    public Internet internet;
    public Results results;
    public LinkedBlockingQueue<Page> pagesToCrawl;

    public Crawler(File fname) {
        results = new Results();
        pagesToCrawl = new LinkedBlockingQueue<Page>();
        connectToInternet(fname);
    }

    public class Internet {
        public Page[] pages;
    }

    public class Page {
        public String address;
        public String[] links;
    }

    public class Results {
        HashSet<String> success = new HashSet<String>();
        HashSet<String> skipped = new HashSet<String>();
        HashSet<String> error = new HashSet<String>();

        public String toString() {
            return
            "\nSuccess:\n" + success.toString() +
            "\nSkipped:\n" + skipped.toString() +
            "\nError:\n" + error.toString() +
            "\n";
        }
    }

    public void connectToInternet(File fname) {
        try {
            FileReader reader = new FileReader(fname);
            parseInternet(reader);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    public void parseInternet(Reader reader) {
        try {
            Gson gson = new Gson();
            internet = gson.fromJson(reader, Internet.class);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Results call() {
        Page firstPage = internet.pages[0];
        crawlPage(firstPage);
        return results;
        // System.out.println(results);
    }

    public void crawlPage(Page startPage) {
        pagesToCrawl.add(startPage);
        for (Page p = pagesToCrawl.poll(); p != null; p = pagesToCrawl.poll()) {
            results.success.add(p.address);
            crawlLinks(p.links);
        }
    }

    public void crawlLinks(String[] links) {
        Page pageFound = null;
        for (String link : links) {
            if (!results.success.contains(link)) {
                pageFound = findPage(link);
                if (pageFound == null) {
                    results.error.add(link);
                } else {
                    pagesToCrawl.add(pageFound);
                }
            } else if (!results.skipped.contains(link)) {
                results.skipped.add(link);
            }
        }
    }

    public Page findPage(String targetAddress) {
        Page foundPage = null;
        for (Page page : internet.pages) {
            if (page.address.equals(targetAddress)) {
                foundPage = page;
            }
        }
        return foundPage;
    }
}
