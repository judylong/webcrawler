# Web Crawler
## Quickstart
Download and cd into this repository.

To compile:
```
$ javac -cp .:./gson-2.8.6-SNAPSHOT.jar Crawler.java CrawlerTest.java InternetCrawl.java
```

To run tests:
```
$ java -cp .:./gson-2.8.6-SNAPSHOT.jar CrawlerTest
```

To run program:
```
$ java -cp .:./gson-2.8.6-SNAPSHOT.jar InternetCrawl
```
## Notes
This is a web crawler that will returns an ArrayList of Results objects. A Results object has publicly accessible instance variables: success, skipped, and error. Each is a HashSet holding strings that represent addresses of pages.

## Assumptions
- Internet files are in json format and located in the directory named 'internet'.
- The json within an internet file is structured such that each page within the list of pages always has a unique address.

## Languages, External Libraries/Frameworks used:
- Java 8
- [gson](https://github.com/google/gson) 
