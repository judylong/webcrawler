import java.util.ArrayList;
import java.io.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class InternetCrawl {
    public static ArrayList<File> jsonFiles = new ArrayList<File>();

    public static ArrayList<File> collectFiles(File directory) {
        for (File f : directory.listFiles()) {
            if (f.isDirectory()) {
                collectFiles(f);
            } else {
                jsonFiles.add(f);
            }
        }
        return jsonFiles;
    }

    public static void main (String[] args) throws InterruptedException, ExecutionException {
        collectFiles(new File("internet"));
        ArrayList<Future<Crawler.Results>> futureResults = new ArrayList<Future<Crawler.Results>>();

        ExecutorService pool = Executors.newFixedThreadPool(5);
        for (File f: jsonFiles) {
            Callable<Crawler.Results> worker = new Crawler(f);
            Future<Crawler.Results> future = pool.submit(worker);
            futureResults.add(future);
        }
        pool.shutdown();

        for (Future<Crawler.Results> f : futureResults) {
            Crawler.Results r = f.get();
            System.out.println(r);
        }
    }

}
