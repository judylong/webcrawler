import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.io.*;

public class CrawlerTest {
    static Crawler c;

    public static void main (String[] args) {
        testParseInternet();
        testSingleCrawlInternet1();
        testSingleCrawlInternet2();
    }

    public static void testParseInternet() {
        Crawler crawler = new Crawler(new File("./internet/internet1.json"));
    }

    public static void setUpCrawler(String fname) {
        c = new Crawler(new File(fname));
        c.call();
    }

    public static void testSingleCrawlInternet1() {
        System.out.println("----------TEST INTERNET 1-------------");

        String expected = "";
        HashSet<String> expectedSuccess = new HashSet<String>(Arrays.asList("http://foo.bar.com/p1", "http://foo.bar.com/p2", "http://foo.bar.com/p4", "http://foo.bar.com/p5", "http://foo.bar.com/p6"));
        HashSet<String> expectedSkipped = new HashSet<String>(Arrays.asList("http://foo.bar.com/p2", "http://foo.bar.com/p4","http://foo.bar.com/p1", "http://foo.bar.com/p5"));
        HashSet<String> expectedError = new HashSet<String>(Arrays.asList("http://foo.bar.com/p3", "http://foo.bar.com/p7"));

        setUpCrawler("./internet/internet1.json");
        assertEquals(expectedSuccess, c.results.success);
        assertEquals(expectedSkipped, c.results.skipped);
        assertEquals(expectedError, c.results.error);
    }

    public static void testSingleCrawlInternet2() {
        System.out.println("----------TEST INTERNET 2-------------");
        String expected = "";
        HashSet<String> expectedSuccess = new HashSet<String>(Arrays.asList("http://foo.bar.com/p1", "http://foo.bar.com/p2", "http://foo.bar.com/p3", "http://foo.bar.com/p4", "http://foo.bar.com/p5"));
        HashSet<String> expectedSkipped = new HashSet<String>(Arrays.asList("http://foo.bar.com/p1"));
        HashSet<String> expectedError = new HashSet<String>();

        setUpCrawler("./internet/internet2.json");
        assertEquals(expectedSuccess, c.results.success);
        assertEquals(expectedSkipped, c.results.skipped);
        assertEquals(expectedError, c.results.error);
    }

    public static void assertEquals(HashSet<String> expected, HashSet<String> actual) {
        String res = (expected.equals(actual)) ? "pass" : "fail : Expected:\n " + expected.toString() + "\nActual:\n" + actual.toString();
        System.out.println(res);
    }
}
